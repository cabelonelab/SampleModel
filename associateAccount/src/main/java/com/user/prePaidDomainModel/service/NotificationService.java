package com.user.prePaidDomainModel.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.user.prePaidDomainModel.domain.request.NotificationRequest;
import com.user.prePaidDomainModel.entity.Customer;
import com.user.prePaidDomainModel.entity.Notification;
import com.user.prePaidDomainModel.repository.CustomerRepository;
import com.user.prePaidDomainModel.repository.NotificationRepository;


@Service
public class NotificationService {
@Autowired
	private NotificationRepository repository;
@Autowired
private CustomerRepository customerRepository;
	
/*	@Autowired
	public NotificationService(NotificationRepository repository, CustomerRepository customerRepository) {
		this.repository = repository;
		this.customerRepository=customerRepository;
	}
*/
	public void sendNotification(NotificationRequest request) {
		Notification notification = new Notification();
		notification.setContant(request.getContent());
		notification.setCreatedAt(new Date());
		notification.setNotificationType(request.getNotificationType());
		notification.setTlm(new Date());

		Optional<Customer> customer = customerRepository.findById(request.getCustomerId());
		if (customer != null) {
			notification.setCustomer(customer.get());
		}
		repository.save(notification);
	}

	public Notification getNotification(long notificationId) {
		Optional<Notification> notification = repository.findById(notificationId);
		if (notification != null) {
			return repository.findById(notificationId).get();
		}
		return null;
	}

	public List<Notification> getNotificationsByCustomer(long customerId) {
		return repository.findByCustomerId(customerId);
	}
	
}
