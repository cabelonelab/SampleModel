package com.user.prePaidDomainModel.entity;

public enum NotificationType {

	Email, SMS, IVR_Phone, IN_Browser;

}
