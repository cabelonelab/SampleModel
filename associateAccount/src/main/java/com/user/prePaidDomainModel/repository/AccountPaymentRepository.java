package com.user.prePaidDomainModel.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.user.prePaidDomainModel.entity.Account;

@Repository
public interface AccountPaymentRepository extends CrudRepository<Account, Long >{
	

}
